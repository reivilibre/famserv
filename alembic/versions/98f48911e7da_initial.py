"""initial

Revision ID: 98f48911e7da
Revises: 
Create Date: 2018-09-16 00:03:47.730284

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '98f48911e7da'
down_revision = None
branch_labels = None
depends_on = None

INVITATION_KEY_MAX_LENGTH = 8
USERID_MAX_LENGTH = 24


def upgrade():
    op.create_table(
        "users",
        sa.Column("userid", sa.String(length=USERID_MAX_LENGTH), primary_key=True),
        sa.Column("passhash", sa.String(length=255), nullable=True),
    )

    op.create_table(
        "invitation_keys",
        sa.Column("key", sa.String(length=INVITATION_KEY_MAX_LENGTH), primary_key=True),
        sa.Column("key_human", sa.String(length=32), nullable=False),
        sa.Column("creator", sa.String(length=USERID_MAX_LENGTH), sa.ForeignKey('users.userid'), nullable=False),
        sa.Column("expiry_timestamp", sa.BigInteger(), nullable=False),
    )


def downgrade():
    op.drop_table("invitation_keys")
    op.drop_table("users")
