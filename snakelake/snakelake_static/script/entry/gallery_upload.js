import qq from '../../node_modules/fine-uploader/fine-uploader/fine-uploader.js';
import '../../node_modules/fine-uploader/fine-uploader/fine-uploader-gallery.css';


document.addEventListener("DOMContentLoaded", function(event) {
	let uploaderElement = document.getElementById("uploader");
	let albumNum = uploaderElement.dataset.albumnum;
	let uploader = new qq.FineUploader({
		element: uploaderElement,
		template: 'qq-template-gallery',
		request: {
			endpoint: '/gallery/upload/handler'
		},
		retry: {
			enableAuto: true // defaults to false
		},
		chunking: {
			enabled: true,
			concurrent: {
				enabled: true
			},
			success: {
				endpoint: "/gallery/upload/chunksdone"
			}
		},
		validation: {
			allowedExtensions: ['jpeg', 'jpg'],
			sizeLimit: 42 * 1024 * 1024,
			minSizeLimit: 8192,
		},
		callbacks: {
			onSubmit: function(fileId, fileName) {
				console.log("submission");
				uploader.setParams({
					'albumnum': albumNum
				}, fileId);
			}
		}
	});
	
	//console.log("uploader", uploader);
	console.log("albumNum!", albumNum);
	window.fuploader = uploader;
});
