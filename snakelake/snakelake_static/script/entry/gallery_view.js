import PhotoSwipe from 'photoswipe'
import PhotoSwipeUI_Default from 'photoswipe/dist/photoswipe-ui-default'

import 'photoswipe/src/css/main.scss';
import 'photoswipe/src/css/default-skin/default-skin.scss';

import * as GSI from '../pieces/gallery_selection_impl.js';

window.gallery_selection = GSI.state;

var slideCache = null;


/// source: PhotoSwipe project.
function photoswipeParseHash() {
	var hash = window.location.hash.substring(1),
	params = {};
	
	var vars = hash.split('&');
	for (var i = 0; i < vars.length; i++) {
		var pair = vars[i].split('=');  
		if (pair.length < 2) {
			continue;
		}           
		params[pair[0]] = pair[1];
	}
	
	return params;
}

function openPhotoSwipe(photonum, fromUrl) {
	var pswpElement = document.querySelectorAll('.pswp')[0];
	
	var options = {
		index: slideCache.indices[photonum],
		history: true,
		galleryPIDs: true,
		showHideOpacity: true,
		getThumbBoundsFn: false,
	};
	
	if (fromUrl) {
		options.showAnimationDuration = 0;
	}
	
	var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, slideCache.items, options);
	gallery.init();
}

function findSlides() {
	var result = {
		items: [],
		indices: {},
	};
	
	var photos = document.querySelectorAll('a.photo[data-photum]');
	var photosLen = photos.length;
	for (let i = 0; i < photosLen; ++i) {
		let photoAnchor = photos[i];
		result.indices[photoAnchor.dataset.photum.substring(1)] = i;
		let caption = photoAnchor.querySelector('figcaption');
		let size = photoAnchor.dataset.size.split('x');
		result.items.push({ // TODO responsive gallery
			src: photoAnchor.href,
			w: parseInt(size[0]),
			h: parseInt(size[1]),
			title: caption.textContent,
			pid: photoAnchor.dataset.photum.substring(1),
		});
	}
	
	return result;
}

// nasty!!! do not use DOMContentLoaded — causes indeterministic failure of laying out the slides (maybe if CSS not yet loaded?)
//document.addEventListener('DOMContentLoaded', function(evt) {
window.addEventListener('load', function(evt) {
	slideCache = findSlides();
	
	GSI.initialise();
	
	GSI.state.fallback = function(node, evt) {
		if (node.dataset.photum.substring(0, 1) == 'p') {
			evt.preventDefault();
			console.log("open via fallback");
			openPhotoSwipe(node.dataset.photum.substring(1));
		}
	};
	
	var hashData = photoswipeParseHash();
	if (hashData.pid) {
		console.log("open via pid");
		openPhotoSwipe(hashData.pid, true);
	}
});
