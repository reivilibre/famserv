// TODO import PhotoSwipe here?


// PHOTos or albUMS
var photums = [];
var selected = new Set();
var photumEles = {};
export var state = {
	selectionModeEnabled: false,
	rangeSelection: false,
	rangeSelectionFirstIndex: null,
	fallback: null,
};

// In practice, photum IDs look like 'a123' (album) or 'p123' (photo)

function photum_onClick(node, evt) {
	window.lastevt = evt;
	if (state.selectionModeEnabled) {
		evt.preventDefault();
		var photumId = node.dataset.photum;
		
		if (state.rangeSelection) {
			var photumIndex = photums.indexOf(photumId);
			if (state.rangeSelectionFirstIndex !== null) {
				// select the entire range!
				let min = Math.min(state.rangeSelectionFirstIndex, photumIndex);
				let max = Math.max(state.rangeSelectionFirstIndex, photumIndex);
				let whetherToSelect = selected.has(photums[state.rangeSelectionFirstIndex]);
				for (var i = min; i <= max; ++i) {
					console.log("WAT", i);
					var photumId = photums[i];
					var photumNode = photumEles[photumId];
					if (whetherToSelect) {
						photumNode.classList.add('selected');
						selected.add(photumId);
					} else {
						photumNode.classList.remove('selected');
						selected.delete(photumId);
					}
					state.rangeSelectionFirstIndex = null;
				}
			} else {
				state.rangeSelectionFirstIndex = photumIndex;
				if (selected.has(photumId)) {
					node.classList.remove('selected');
					selected.delete(photumId);
				} else {
					node.classList.add('selected');
					selected.add(photumId);
				}
			}
		} else {	
			if (selected.has(photumId)) {
				node.classList.remove('selected');
				selected.delete(photumId);
			} else {
				node.classList.add('selected');
				selected.add(photumId);
			}
		}
	} else if (state.fallback) {
		state.fallback(node, evt);
	}
}

export function initialise() {
	var photumNodes = document.querySelectorAll('a[data-photum]');
	var pnl = photumNodes.length;
	
	for (var i = 0; i < pnl; ++i) {
		// let is very important here as it otherwise disturbs the arrow function's closure
		let photumNode = photumNodes[i];
		photumNode.addEventListener('click', evt => photum_onClick(photumNode, evt));
		photums.push(photumNode.dataset.photum);
		photumEles[photumNode.dataset.photum] = photumNode;
		
		console.log("Registering " + i + " " + photumNode.dataset.photum);
	}
	
	initialiseManagementBar();
}



function initialiseManagementBar() {
	var gmManage = document.getElementById('gm-manage');
	if (gmManage) {
		var gmRange = document.getElementById('gm-range');
		var gmBar = document.getElementById('gm-bar');
		
		gmManage.addEventListener('click', function (event) {
			var newSelMode = !state.selectionModeEnabled;
			state.selectionModeEnabled = newSelMode;
			
			if (newSelMode) {
				gmBar.style.display = 'block';
				gmManage.parentNode.classList.add('nav-active');
			} else {
				gmBar.style.display = 'none';
				gmManage.parentNode.classList.remove('nav-active');
			}
		});
		
		gmRange.addEventListener('click', function (event) {
			var newRangeMode = !state.rangeSelection;
			state.rangeSelection = newRangeMode;
			
			if (newRangeMode) {
				gmRange.parentNode.classList.add('nav-active');
				gmRange.textContent = '(range)';
			} else {
				gmRange.parentNode.classList.remove('nav-active');
				gmRange.textContent = '(single)';
			}
		});
	}
}
