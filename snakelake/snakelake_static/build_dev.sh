#!/bin/sh

set -eu
echo todo cssnano
mkdir -p ../snakelake/static/

#yarn run parcel build 'style/entry/*.scss'
yarn run parcel build 'script/entry/*.js' --no-minify

rm -r ../snakelake/static/parcel
cp -r dist -T ../snakelake/static/parcel

sass $@ style/entry/light.scss ../snakelake/static/light.css
sass $@ style/entry/gallery_dark.scss ../snakelake/static/gallery_dark.css
