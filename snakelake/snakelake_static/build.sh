#!/bin/sh

set -eu
mkdir -p ../snakelake/static/

#yarn run parcel build 'style/entry/*.scss'
yarn run parcel build 'script/entry/*.js'

rm -r ../snakelake/static/parcel
cp -r dist -T ../snakelake/static/parcel

#sass $@ style/entry/light.scss ../snakelake/static/light.css
sass $@ style/entry/light.scss | yarn run --silent postcss > ../snakelake/static/light.css
# sass $@ style/entry/gallery_dark.scss ../snakelake/static/gallery_dark.css
sass $@ style/entry/gallery_dark.scss | yarn run --silent postcss > ../snakelake/static/gallery_dark.css

