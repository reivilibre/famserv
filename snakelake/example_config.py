REDIS_ADDRESS = 'redis://127.0.0.1'
REDIS_XTSSO_DB = 0
REDIS_PASSWORD = ''
REDIS_POOL_MAX_SIZE = 2
REDIS_EXPIRY = 7 * 86400  # 7 days

PG_HOST = '127.0.0.1'  # or socket.
PG_USER = ''
PG_PASSWORD = ''
PG_DATABASE = ''

SECRET_KEY = ''

TITLE = "Snake Lake"
COPY_OPERATORS = 'The SiteOperator(s)'
COPY_YEAR = 1970

# IP_ADDRESS_HEADER = 'X-Forwarded-For'
IP_ADDRESS_HEADER = None

INTERNAL_ERROR_ADVICE = 'Please contact the operator via e-mail: xyz@xyz.xyz'

XTSSO_COOKIE_KEY = 'xtsso'
XTSSO_COOKIE_MAXAGE = 7 * 86400  # 7 days
XTSSO_COOKIE_PATH = '/'
XTSSO_COOKIE_DOMAIN = '.example.org'
XTSSO_COOKIE_SECURE = True
XTSSO_COOKIE_HTTPONLY = True
XTSSO_ENDPOINT = 'https://example.org'

SELF_URL_PREFIX = 'https://snakelake.example.org'

# EMAIL_LAMBDA = lambda userid: f'{userid}@example.org'
EMAIL_LAMBDA = None

RETURN_LINK = ('Return to Example.org', 'https://example.org')


def default_avatar_url(userid):
    if userid == 'plasma':
        return '/static/plasma_avatar.png'
    elif userid == 'lava':
        return '/static/lava_avatar.png'
    else:
        return f'https://example.org/avatar_for/{userid}.png'


DEFAULT_AVATAR_URL = default_avatar_url
# DEFAULT_AVATAR_URL = lambda userid: f'https://example.org/avatar_for/{userid}.png'
PRIVACY_URL = 'https://example.org/about/privacy'
CONTACT_URL = 'https://example.org/about/contact'

SELF_URL = 'https://sl.example.org'

PHOTO_UPLOAD_DIR = '/path/to/uploads'
# make sure indexing is disabled for the photo_url dir.
PHOTO_ROOT_URL = '/_gallery'
PHOTO_CLEANUP_DELAY_INITIAL = 60
PHOTO_CLEANUP_INTERVAL = 86400
# NOTIMPL URI Root (e.g. /internal_images) for serving photos with X-Accel-Redirect and nginx; set to None to disable.
# NOTIMPL NGINX_XACCEL_ROOT = None
