"""galleryfix

Revision ID: c403d60a8edb
Revises: f54b5b6ab6e5
Create Date: 2018-10-12 18:18:35.345111

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql import table, column

# revision identifiers, used by Alembic.
revision = 'c403d60a8edb'
down_revision = 'f54b5b6ab6e5'
branch_labels = None
depends_on = None

DEFAULT_GALLERY_QUOTA = 1 * 1024 * 1024 * 1024  # 1 GiB?


def upgrade():
    user_table = table(
        'user',
        column('gallery_quota', sa.Integer())
    )

    op.execute(
        user_table.update().values({'gallery_quota': op.inline_literal(DEFAULT_GALLERY_QUOTA)})
    )
    
    op.alter_column("user", "gallery_quota", nullable=False, server_default=f'{DEFAULT_GALLERY_QUOTA}')


def downgrade():
    op.alter_column("user", "gallery_quota", nullable=True, server_default=None)
