"""gallery

Revision ID: f54b5b6ab6e5
Revises: fce4c10e27c4
Create Date: 2018-09-25 20:16:21.043941

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql import table, column

# revision identifiers, used by Alembic.
revision = 'f54b5b6ab6e5'
down_revision = 'fce4c10e27c4'
branch_labels = None
depends_on = None

GALLERY_TITLE_LENGTH = 128
GALLERY_UNLISTED_BASE64_LENGTH = 12
PHOTO_MAX_RETAINED_FILENAME_LEN = 24
PHOTO_HASH_RAW_LENGTH = 18
PHOTO_HASH_BASE64_LENGTH = 4 * (PHOTO_HASH_RAW_LENGTH / 3)

# default user quota. Note that the quota does not account for thumbnails and such, so you should expect
# about a 20% inflation of this to the real max disk usage.
DEFAULT_GALLERY_QUOTA = 1 * 1024 * 1024 * 1024  # 1 GiB?


def upgrade():
    op.create_table(
        "album",
        sa.Column("albumnum", sa.Integer(), primary_key=True, autoincrement=True),
        sa.Column("parent_albumnum", sa.Integer(), sa.ForeignKey('album.albumnum'), index=True),
        sa.Column("name", sa.String(length=GALLERY_TITLE_LENGTH), nullable=False),
        sa.Column("unlisted_token", sa.String(length=GALLERY_UNLISTED_BASE64_LENGTH), nullable=False),
        sa.Column("owner_usernum", sa.Integer(), sa.ForeignKey('user.usernum'), nullable=False)
    )

    op.create_table(
        "photograph",
        sa.Column("photonum", sa.Integer(), primary_key=True, autoincrement=True),
        sa.Column("albumnum", sa.Integer(), sa.ForeignKey("album.albumnum"), nullable=False),
        sa.Column("width", sa.Integer(), nullable=False),
        sa.Column("height", sa.Integer(), nullable=False),
        # original filename shall exclude the file extension.
        sa.Column("original_filename", sa.String(length=PHOTO_MAX_RETAINED_FILENAME_LEN), nullable=False),
        sa.Column("caption", sa.Text(), nullable=False),
        sa.Column("photohash", sa.String(length=PHOTO_HASH_BASE64_LENGTH), nullable=False),
        sa.Column("timestamp", sa.Integer(), nullable=False, index=True),
        sa.Column("filesize", sa.Integer(), nullable=False),
    )

    op.add_column(
        "album",
        sa.Column("cover_photonum", sa.Integer(), sa.ForeignKey("photograph.photonum"), nullable=True)
    )

    op.add_column(
        "user",
        sa.Column("gallery_quota", sa.Integer(), nullable=True)
    )

    user_table = table(
        'user',
        column('gallery_quota', sa.Integer())
    )


def downgrade():
    op.drop_column("album", "cover_photonum")
    op.drop_table("photograph")
    op.drop_table("album")
