"""initial

Revision ID: fce4c10e27c4
Revises: 
Create Date: 2018-09-20 22:49:12.328061

"""
from alembic import op
import sqlalchemy as sa

# from sqlalchemy.schema import CreateSequence, Sequence, DropSequence

# revision identifiers, used by Alembic.
revision = 'fce4c10e27c4'
down_revision = None
branch_labels = None
depends_on = None

# inherited from Identity
USERID_MAX_LENGTH = 24
DISPLAY_NAME_MAX_LENGTH = 32
TOPPOST_TITLE_LENGTH = 128


def upgrade():
    op.create_table(
        "user",
        sa.Column("userid", sa.String(length=USERID_MAX_LENGTH), unique=True, nullable=False),
        sa.Column("usernum", sa.Integer(), nullable=False, primary_key=True, autoincrement=True),
        sa.Column("display_name", sa.String(length=DISPLAY_NAME_MAX_LENGTH), nullable=False),
    )

    op.create_table(
        "set",
        sa.Column("setnum", sa.Integer(), primary_key=True, nullable=False, autoincrement=True),
        sa.Column("owner_usernum", sa.Integer(), sa.ForeignKey('user.usernum'),
                  nullable=False),
        sa.Column("name", sa.String(length=DISPLAY_NAME_MAX_LENGTH), nullable=True)
    )

    op.add_column(
        "user",
        sa.Column("primary_setnum", sa.Integer(), sa.ForeignKey('set.setnum', deferrable=True, initially='DEFERRED'),
                  nullable=False)
    )

    op.create_table(
        "set_membership",
        sa.Column("setnum", sa.Integer(), sa.ForeignKey('set.setnum'), nullable=False, index=True),
        sa.Column("usernum", sa.Integer(), sa.ForeignKey('user.usernum'), nullable=False, index=True),
        sa.PrimaryKeyConstraint('setnum', 'usernum')
    )

    op.create_table(
        "thread",
        sa.Column("threadnum", sa.Integer(), nullable=False, primary_key=True, autoincrement=True),
        sa.Column("poster_usernum", sa.Integer(), nullable=False, index=True),
        sa.Column("title", sa.String(length=TOPPOST_TITLE_LENGTH), nullable=False),
        sa.Column("visible_to_setnum", sa.Integer(), sa.ForeignKey('set.setnum'), nullable=False, index=True),
    )

    op.create_table(
        "post",
        sa.Column("postnum", sa.Integer(), nullable=False, primary_key=True, autoincrement=True),
        sa.Column("poster_usernum", sa.Integer(), nullable=False),
        sa.Column("threadnum", sa.Integer(), sa.ForeignKey('thread.threadnum'), nullable=False, index=True),
        sa.Column("parent_postnum", sa.Integer(), nullable=True),
        sa.Column("content_type", sa.SmallInteger(), nullable=False),
        sa.Column("content", sa.Text(), nullable=False),
        sa.Column("posted_at", sa.Integer(), nullable=False),
    )

    # for now, one set per post.
    # op.create_table(
    #     "post_visibility",
    #     sa.Column("setnum", sa.Integer(), sa.ForeignKey('set.setnum'), nullable=False, index=True),
    #     sa.Column("postnum", sa.Integer(), sa.ForeignKey('post.postnum'), nullable=False, index=True),
    #     sa.PrimaryKeyConstraint('setnum', 'postnum')
    # )


def downgrade():
    op.drop_table("post")
    op.drop_table("thread")
    op.drop_table("set_membership")
    op.drop_column("user", "primary_setnum")
    op.drop_table("set")
    op.drop_table("user")
    # op.drop_table("post_visibility")
