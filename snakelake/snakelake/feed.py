from quart import Blueprint, request, current_app, g, make_response, render_template, redirect, \
    render_template_string, session, flash

import arrow

from wtforms_async import StringField, TextAreaField, SelectField, Form
from wtforms_async.validators import Length, InputRequired
from wtforms_async.csrf.core import CSRFTokenField
from wtforms_async.csrf.session import SessionCSRF
from . import util
from . import reference

feed_blueprint = Blueprint("feed", __name__)


@feed_blueprint.route('/')
@util.login_required
async def root():
    # for suspected efficiency reasons, we will perform usernum lookups afterwards,
    # deviating from my prior feed.sql
    # In this version, posts can also only be made to one set at a time.
    thread_query = """
        (SELECT t.threadnum, t.poster_usernum, t.title, t.visible_to_setnum
            FROM "thread" t
            LEFT JOIN "set_membership" sm ON sm.setnum = t.visible_to_setnum
            WHERE sm.usernum = $1)
        UNION (SELECT t.threadnum, t.poster_usernum, t.title, t.visible_to_setnum
            FROM "thread" t
            WHERE t.poster_usernum = $1)
        ORDER BY threadnum DESC
        LIMIT 25 OFFSET 0
    """

    # shall we prepare these queries or use WHERE INs?
    # neither: try WHERE ___ = ANY ($)
    post_query = """
        SELECT postnum, parent_postnum, threadnum, poster_usernum, content_type, content, posted_at
        FROM "post" p
        WHERE threadnum = ANY ($1)
        ORDER BY p.postnum ASC
    """

    user_query = """
        SELECT usernum, userid, display_name
        FROM "user"
        WHERE usernum = ANY ($1)
    """

    async with current_app.pg_pool.acquire() as pg:
        thread_rows = await pg.fetch(thread_query, g.usernum)
        threadnums = [x['threadnum'] for x in thread_rows]
        post_rows = await pg.fetch(post_query, threadnums)
        usernums = {x['poster_usernum'] for x in post_rows}
        user_rows = await pg.fetch(user_query, usernums)

        sets = dict()
        for setnum, set_name in await pg.fetch('SELECT setnum, name FROM "set" WHERE owner_usernum=$1', g.usernum):
            if set_name is None:
                sets[setnum] = 'All contacts'
            else:
                sets[setnum] = set_name

    user_mapping = {x['usernum']: dict(x) for x in user_rows}

    for user in user_mapping.values():
        user['avatar_url'] = current_app.config['DEFAULT_AVATAR_URL'](user['userid'])

    threads_in_order = [dict(thread) for thread in thread_rows]

    for thread in threads_in_order:
        thread['main'] = None
        thread['childrenmap'] = {}

    threads_by_id = {t['threadnum']: t for t in threads_in_order}

    for post in post_rows:
        thread_dict = threads_by_id[post['threadnum']]
        parentnum = post['parent_postnum']
        if parentnum is not None:
            if parentnum not in thread_dict['childrenmap']:
                thread_dict['childrenmap'][parentnum] = []
            thread_dict['childrenmap'][parentnum].append(dict(post))
        else:
            thread_dict['main'] = dict(post)

    return await render_template('feed_root.html', threads=threads_in_order, users=user_mapping,
                                 sets=sets, validity=arrow.utcnow().timestamp)


@feed_blueprint.route('/pub/<int:parent>', methods=['GET', 'POST'])
@util.login_required
async def new_subpost(parent):
    class NewSubpostForm(util.ProtectedForm):
        message = TextAreaField('Message', filters=[util.strip_whitespace], validators=[Length(min=1, max=2048)])

    async with current_app.pg_pool.acquire() as pg:
        post = await pg.fetchrow('SELECT p.postnum, p.posted_at, t.title, p.parent_postnum, p.content_type,'
                                 ' p.content, u.display_name, u.userid, p.threadnum'
                                 ' FROM post p'
                                 ' INNER JOIN thread t ON p.threadnum = t.threadnum'
                                 ' INNER JOIN "user" u ON p.poster_usernum = u.usernum'
                                 ' LEFT JOIN set_membership sm ON sm.setnum = t.visible_to_setnum'
                                 ' WHERE p.postnum = $1'
                                 ' AND (sm.usernum = $2 OR p.poster_usernum = $2 OR t.poster_usernum = $2)', parent, g.usernum)

        if post is None:
            # not found.
            await flash("Post not found")
            return redirect('/', status_code=303)

        post = dict(post)
        post['avatar_url'] = current_app.config['DEFAULT_AVATAR_URL'](post['userid'])

    form = NewSubpostForm(await request.form)

    if request.method == "POST" and await form.validate():
        async with current_app.pg_pool.acquire() as pg:
            timestamp = arrow.utcnow().timestamp
            await pg.fetchrow('INSERT INTO post (poster_usernum, threadnum, parent_postnum, content_type,'
                              ' content, posted_at) VALUES ($1, $2, $3, $4, $5, $6)',
                              g.usernum, post['threadnum'], post['postnum'], reference.ContentTypes.TEXT_PLAIN.value,
                              form.message.data, timestamp)
            await flash('Message published')
            return redirect('/', status_code=303)

    return await render_template('feed_pubreply.html', post=post, form=form)


@feed_blueprint.route('/pub', methods=['GET', 'POST'])
@util.login_required
async def new_top_level_post():
    async with current_app.pg_pool.acquire() as pg:
        sets = []
        for setnum, set_name in await pg.fetch('SELECT setnum, name FROM "set" WHERE owner_usernum=$1', g.usernum):
            if set_name is None:
                sets.append((str(setnum), 'All contacts'))
            else:
                sets.append((str(setnum), set_name))

    class NewTopLevelPostForm(util.ProtectedForm):
        title = StringField('Brief Title', validators=[Length(max=128)])
        # may eventually be replaced with a Slate editor, according to user config
        message = TextAreaField('Message', validators=[Length(min=3, max=2048)])
        visible_to = SelectField('Visible to', validators=[InputRequired()], choices=sets)

    form = NewTopLevelPostForm(await request.form)

    if request.method == 'POST' and await form.validate():
        async with current_app.pg_pool.acquire() as pg:
            timestamp = arrow.utcnow().timestamp
            async with pg.transaction():
                threadnum_row = await pg.fetchrow('INSERT INTO thread'
                                                  ' (title, visible_to_setnum, poster_usernum)'
                                                  ' VALUES ($1, $2, $3) RETURNING threadnum',
                                                  form.title.data, int(form.visible_to.data), g.usernum)
                await pg.fetchrow('INSERT INTO post (poster_usernum, threadnum, parent_postnum, content_type,'
                                  ' content, posted_at) VALUES ($1, $2, NULL, $3, $4, $5)',
                                  g.usernum, threadnum_row[0], reference.ContentTypes.TEXT_PLAIN.value,
                                  form.message.data.replace('\r\n', '\n'), timestamp)
            await flash('Message published')
            return redirect('/', status_code=303)

    return await render_template('feed_pub.html', form=form)
