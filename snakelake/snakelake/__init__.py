from quart import Quart, g, render_template, current_app, request, send_from_directory
from quart.wrappers import Response
import asyncio
import aioredis
import asyncpg
import concurrent.futures
from . import feed, sets, profile, gallery, formatting, util
from .gallery import upload
from .gallery import processing as gallery_processing

OFFLINE = False

loop = asyncio.get_event_loop()

app = Quart(__name__)

app.config.from_envvar('SL_CONFIG')

app.register_blueprint(feed.feed_blueprint, '')
app.register_blueprint(sets.sets_blueprint, '/sets')
app.register_blueprint(profile.profile_blueprint, '')
app.register_blueprint(gallery.gallery_blueprint, '/gallery')
app.register_blueprint(upload.upload_blueprint, '/gallery/upload')


# create a redis connection
# this adds it to the first layer of g, which will not be lost between requests
# changes to g during a request are not persisted between requests, though.
async def register_pools():
    async with app.app_context():
        from . import util
        util.ProtectedForm.Meta.csrf_secret = current_app.config['SECRET_KEY']
        cfg = app.config
        current_app.redis_pool = await aioredis.create_redis_pool(cfg['REDIS_ADDRESS'], db=cfg['REDIS_XTSSO_DB'],
                                                                  password=cfg['REDIS_PASSWORD'], minsize=1,
                                                                  maxsize=cfg['REDIS_POOL_MAX_SIZE'],
                                                                  encoding='utf-8')
        current_app.pg_pool = await asyncpg.create_pool(host=cfg['PG_HOST'], database=cfg['PG_DATABASE'],
                                                        user=cfg['PG_USER'],
                                                        password=cfg['PG_PASSWORD'])


async def register_filters():
    async with app.app_context():
        current_app.jinja_env.filters['in_timezone'] = util.in_timezone
        current_app.jinja_env.filters['relativise_time'] = util.relativise_time
        current_app.jinja_env.filters['humane_dateonly'] = util.humane_dateonly
        current_app.jinja_env.filters['paragraphise'] = formatting.paragraphise
        current_app.jinja_env.filters['specified_formatting'] = formatting.specified_formatting


@app.before_first_request
async def app_startup():
    print("STARTUP")
    if not OFFLINE:
        await register_pools()
    await register_filters()

    # executor = concurrent.futures.ProcessPoolExecutor(max_workers=2)
    # gives AssertionError: daemonic processes are not allowed to have children in prod
    # FIXME
    executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
    current_app.future_executor = executor

    asyncio.ensure_future(gallery_processing.scheduled_cancel_old(app))


@app.route('/about/credits')
async def attributions():  # credits is a built-in Python name
    return await render_template('index_credits.html')


@app.route('/_gallery/<filename>')
async def demo_gallery(filename):
    """
    In real usage, this should be replaced with reverse proxy rules.
    """
    return await send_from_directory(current_app.config['PHOTO_UPLOAD_DIR'], filename)


if not OFFLINE:
    @app.before_request
    async def before_request_login():
        # could add an exception here. if request.endpoint[0:2] != '/_':
        if current_app.config['XTSSO_COOKIE_KEY'] in request.cookies:
            xtsso_sesskey = request.cookies[current_app.config['XTSSO_COOKIE_KEY']]
            redis: aioredis.Redis = current_app.redis_pool
            g.cid = await redis.hget(f'session:{xtsso_sesskey}', 'cid')
        else:
            g.cid = None

        # get the user number and such
        if g.cid is not None:
            async with current_app.pg_pool.acquire() as pg:
                extra_userdata = await pg.fetchrow('SELECT usernum, primary_setnum, gallery_quota FROM "user"'
                                                   ' WHERE userid=$1', g.cid)
                if extra_userdata is not None:
                    g.usernum = extra_userdata['usernum']
                    g.primary_setnum = extra_userdata['primary_setnum']
                    g.gallery_quota = extra_userdata['gallery_quota']
                else:
                    async with pg.transaction():
                        usernum_row = await pg.fetchrow('INSERT INTO "user" (userid, display_name, primary_setnum)'
                                                        ' VALUES ($1, $2, -42) RETURNING usernum', g.cid, g.cid.title())
                        g.usernum = usernum_row[0]
                        setnum_row = await pg.fetchrow('INSERT INTO "set" (owner_usernum, name)'
                                                       ' VALUES ($1, NULL) RETURNING setnum', g.usernum)
                        g.primary_setnum = setnum_row[0]
                        await pg.execute('UPDATE "user" SET primary_setnum=$1 WHERE usernum=$2', g.primary_setnum,
                                         g.usernum)

                        g.gallery_quota = 0  # they will inherit the server default quota on next page load

        g.timezone = 'Europe/London'  # yuck but temporary until I can get my favourite UTC back.


    @app.after_request
    async def after_request_login(response):
        cfg = current_app.config
        if cfg['XTSSO_COOKIE_KEY'] in request.cookies:
            if g.cid is None:
                response.set_cookie(cfg['XTSSO_COOKIE_KEY'], '', max_age=-42,
                                    path=cfg['XTSSO_COOKIE_PATH'], domain=cfg['XTSSO_COOKIE_DOMAIN'],
                                    secure=cfg['XTSSO_COOKIE_SECURE'],
                                    httponly=cfg['XTSSO_COOKIE_HTTPONLY'])
            else:
                response.set_cookie(cfg['XTSSO_COOKIE_KEY'], request.cookies[cfg['XTSSO_COOKIE_KEY']],
                                    max_age=cfg['XTSSO_COOKIE_MAXAGE'],
                                    path=cfg['XTSSO_COOKIE_PATH'], domain=cfg['XTSSO_COOKIE_DOMAIN'],
                                    secure=cfg['XTSSO_COOKIE_SECURE'],
                                    httponly=cfg['XTSSO_COOKIE_HTTPONLY'])
        return response
else:
    @app.before_request
    async def before_request_login():
        g.timezone = 'Europe/London'
