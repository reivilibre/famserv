from quart import Blueprint, request, current_app, g, make_response, render_template, redirect, \
    render_template_string, flash
from wtforms_async import Form, FieldList, FormField, SelectField, HiddenField, StringField
from wtforms_async.validators import Length
from . import util
import traceback
from asyncpg.exceptions import UniqueViolationError, ForeignKeyViolationError

sets_blueprint = Blueprint("sets", __name__)


# 'Sets' are groups of contacts, but not community groups.

@sets_blueprint.route('/')
async def sets_overview():
    # show: link to contacts, all contactsets, REQUESTS

    incoming_query = """
        SELECT u.userid, u.usernum, u.display_name
        FROM "user" u
        JOIN "set_membership" sm ON sm.setnum = u.primary_setnum
        WHERE sm.usernum = $1
        AND NOT EXISTS (
                SELECT ism.usernum
                FROM "set_membership" ism
                WHERE ism.usernum=u.usernum AND ism.setnum=$2
            )
        ORDER BY u.userid
    """

    outgoing_query = """
        SELECT u.userid, u.usernum, u.display_name
        FROM "user" u
        JOIN "set_membership" sm ON sm.usernum = u.usernum
        WHERE sm.setnum = $2
        AND NOT EXISTS (
            SELECT 1
            FROM "set_membership" ism
            WHERE ism.setnum=u.primary_setnum
            AND ism.usernum=$1
        )
    """

    async with current_app.pg_pool.acquire() as pg:
        incoming_requests = await pg.fetch(incoming_query, g.usernum, g.primary_setnum)
        outgoing_requests = await pg.fetch(outgoing_query, g.usernum, g.primary_setnum)

        csets = await pg.fetch('SELECT setnum, name FROM set WHERE owner_usernum = $1 AND name IS NOT NULL', g.usernum)

    iform = util.ProtectedForm()
    oform = AddContactForm()

    return await render_template('sets_root.html', incoming=incoming_requests, outgoing=outgoing_requests, iform=iform,
                                 oform=oform, csets=csets)


@sets_blueprint.route('/incoming', methods=['POST'])
async def process_incoming():
    reqform = await request.form
    form = util.ProtectedForm(reqform)

    if await form.validate():
        num_actions = 0
        async with current_app.pg_pool.acquire() as pg:
            for key, value in reqform.items():
                # print(f"key {key}, value {value}")
                if key[0:5] == 'ireq_':
                    # print("ireq!")
                    try:
                        usernum = int(key[5:])
                        # print(f"usernum {usernum}")
                        if value == 'acc':
                            # print("acc!")
                            # request friends or add to contacts
                            try:
                                await pg.execute('INSERT INTO set_membership (setnum, usernum) VALUES ($1, $2)',
                                                 g.primary_setnum, usernum)
                            except UniqueViolationError:
                                pass  # ignore duplicates
                        elif value == 'rej':
                            # delete & reject
                            # remove the deletee from the user's sets
                            await pg.execute('DELETE FROM set_membership WHERE usernum = $2 AND setnum IN '
                                             '(SELECT setnum FROM set WHERE owner_usernum = $1)', g.usernum,
                                             usernum)

                            # also sweep the user out of the deletee's sets
                            await pg.execute('DELETE FROM set_membership WHERE usernum = $2 AND setnum IN '
                                             '(SELECT setnum FROM set WHERE owner_usernum = $1)', usernum,
                                             g.usernum)
                    except (ValueError, ForeignKeyViolationError):
                        print("Based on user-input, bad usernum:")
                        traceback.print_exc()
        return redirect('/sets/', status_code=303)
    else:
        actions = []
        acc = 0
        rej = 0
        for key, value in reqform.items():
            print(key)
            if value == 'acc':
                acc += 1
            elif value == 'rej':
                rej += 1

        if acc == 1:
            actions.append('Accept one contact request')
        elif acc > 1:
            actions.append(f'Accept {acc} contact requests')

        if rej == 1:
            actions.append('Reject one contact request')
        elif rej > 1:
            actions.append(f'Reject {rej} contact requests')

        return await render_template('sets_incoming_resend.html', actions=actions, form=form)


class ContactsetForm(util.ProtectedForm):
    name = StringField('Name of the contact group:', filters=[util.strip_whitespace],
                       validators=[Length(min=1, max=32)])


@sets_blueprint.route('/contactset/new', methods=['GET', 'POST'])
async def create_contactset():
    form = ContactsetForm(await request.form)
    if request.method == 'POST' and await form.validate():
        async with current_app.pg_pool.acquire() as pg:
            setnum_row = await pg.fetchrow('INSERT INTO set (owner_usernum, name) VALUES ($1, $2) RETURNING setnum',
                                           g.usernum, form.name.data)

            return redirect(f'/sets/contactset/{setnum_row[0]}', status_code=303)
    return await render_template('sets_create.html', form=form)


class AddContactForm(util.ProtectedForm):
    userid = StringField('User ID:', filters=[util.lowercase_filter, util.strip_whitespace])


@sets_blueprint.route('/outgoing', methods=['GET', 'POST'])
async def process_outgoing():
    reqform = await request.form
    form = AddContactForm(reqform)

    if request.method == 'POST' and await form.validate():
        async with current_app.pg_pool.acquire() as pg:
            usernum_row = await pg.fetchrow('SELECT usernum FROM "user" WHERE userid = $1', form.userid.data)
            if usernum_row is not None:
                try:
                    await pg.execute('INSERT INTO set_membership (setnum, usernum)'
                                     ' VALUES ($1, $2)', g.primary_setnum, usernum_row[0])
                except UniqueViolationError:
                    # traceback.print_exc()
                    await flash(f"Failed to add {form.userid.data} to contacts — are they already a contact?")
                    return redirect('/sets')

                await flash(f"Added {form.userid.data} to contacts")
                return redirect('/sets')
            else:
                form.userid.errors.append('There is no user with that User ID.')

    return await render_template('sets_outgoing_create.html', form=form)


@sets_blueprint.route('/requests')
def contact_requests():
    pass


@sets_blueprint.route('/contactset/<int:setnum>')
async def contact_group(setnum):
    if setnum == g.primary_setnum:
        await flash('Please manage your "All contacts" contactset through this page.')
        return redirect('/sets/all', status_code=303)

    async with current_app.pg_pool.acquire() as pg:
        cset = await pg.fetchrow('SELECT name, setnum FROM set WHERE setnum=$1 AND owner_usernum=$2',
                                 setnum, g.usernum)
        if cset is None:
            await flash('That contactset does not exist.')
            return redirect('/sets/all', status_code=303)

        members = await pg.fetch("""
            SELECT u.display_name, u.userid, u.usernum
            FROM "set_membership" sm, "user" u
            WHERE u.usernum = sm.usernum AND sm.setnum = $1
        """, setnum)

    return await render_template('sets_show.html', setnum=setnum, cset=cset, members=members)


@sets_blueprint.route('/all')
async def view_all_contacts():
    setnum = g.primary_setnum

    async with current_app.pg_pool.acquire() as pg:
        cset = await pg.fetchrow('SELECT name, setnum FROM set WHERE setnum=$1 AND owner_usernum=$2',
                                 setnum, g.usernum)
        if cset is None:
            await flash('That contactset does not exist.')
            return redirect('/sets/all', status_code=303)

        members = await pg.fetch("""
            SELECT u.display_name, u.userid, u.usernum
            FROM "set_membership" sm
            LEFT JOIN "user" u ON u.usernum = sm.usernum
            WHERE sm.setnum = $1
        """, setnum)

        cset = {'name': 'All contacts'}

    return await render_template('sets_show.html', cset=cset, members=members, is_primary_set=True)


@sets_blueprint.route('/all/edit', methods=['GET', 'POST'])
async def edit_all_contacts():
    setnum = g.primary_setnum

    async with current_app.pg_pool.acquire() as pg:
        cset = await pg.fetchrow('SELECT name, setnum FROM set WHERE setnum=$1 AND owner_usernum=$2',
                                 setnum, g.usernum)
        if cset is None:
            await flash('That contactset does not exist.')
            return redirect('/sets', status_code=303)

        members = await pg.fetch("""
            SELECT u.display_name, u.userid, u.usernum
            FROM "set_membership" sm
            LEFT JOIN "user" u ON u.usernum = sm.usernum
            WHERE sm.setnum = $1
        """, setnum)

    reqform = await request.form
    form = util.ProtectedForm(reqform)

    to_be_deleted = set()
    membernums = {member['usernum'] for member in members}

    if request.method == 'POST' and await form.validate():
        for key in reqform.keys():
            if key[0:3] == 'del':
                try:
                    to_be_deleted.add(int(key[3:]))
                except ValueError:
                    pass  # just ignore it

        # you can only delete members
        to_be_deleted.intersection_update(membernums)

        async with current_app.pg_pool.acquire() as pg:
            # await pg.execute('DELETE FROM set_membership WHERE setnum = $1 AND usernum = ANY ($2)',
            #                  setnum, to_be_deleted)
            # remove the deletee from the user's sets
            await pg.execute('DELETE FROM set_membership WHERE usernum = ANY ($2) AND setnum IN '
                             '(SELECT setnum FROM set WHERE owner_usernum = $1)', g.usernum, to_be_deleted)

            # also sweep the user out of the deletee's sets
            await pg.execute('DELETE FROM set_membership WHERE usernum = $1 AND setnum IN '
                             '(SELECT setnum FROM set WHERE owner_usernum = ANY ($2))', g.usernum, to_be_deleted)

        await flash('Contact group updated')
        return redirect(f'/sets/all', status_code=303)

    # TODO extent the CSRF token to be longer.
    cset = {'name': 'All contacts'}

    # TODO make the form sustain state across CSRF failure (not a WTForm one this time)
    return await render_template('sets_manage.html', form=form, cset=cset, members=members)


@sets_blueprint.route('/contactset/<int:setnum>/edit', methods=['GET', 'POST'])
async def edit_contact_group(setnum):
    if setnum == g.primary_setnum:
        await flash('Please manage your "All contacts" contactset through this page.')
        return redirect('/sets/all', status_code=303)

    async with current_app.pg_pool.acquire() as pg:
        cset = await pg.fetchrow('SELECT name, setnum FROM set WHERE setnum=$1 AND owner_usernum=$2',
                                 setnum, g.usernum)
        if cset is None:
            await flash('That contactset does not exist.')
            return redirect('/sets/all', status_code=303)

        members = await pg.fetch("""
            SELECT u.display_name, u.userid, u.usernum
            FROM "set_membership" sm, "user" u
            WHERE u.usernum = sm.usernum AND sm.setnum = $1
        """, setnum)

        contacts = await pg.fetch("""
            SELECT u.display_name, u.userid, u.usernum
            FROM "set_membership" sm, "user" u
            WHERE u.usernum = sm.usernum AND sm.setnum = $1
        """, g.primary_setnum)

        nonmembers = set(contacts)
        nonmembers.difference_update(members)

    reqform = await request.form
    form = util.ProtectedForm(reqform)

    to_be_added = set()
    to_be_deleted = set()
    # contactnums = {contact['usernum'] for contact in contacts}
    membernums = {member['usernum'] for member in members}
    nonmembernums = {nonmember['usernum'] for nonmember in nonmembers}

    if request.method == 'POST' and await form.validate():
        for key in reqform.keys():
            if key[0:3] == 'add':
                try:
                    to_be_added.add(int(key[3:]))
                except ValueError:
                    pass  # just ignore it
            elif key[0:3] == 'del':
                try:
                    to_be_deleted.add(int(key[3:]))
                except ValueError:
                    pass  # just ignore it

        # you can only add non-members
        to_be_added.intersection_update(nonmembernums)
        # you can only delete members
        to_be_deleted.intersection_update(membernums)

        async with current_app.pg_pool.acquire() as pg:
            for addable in to_be_added:
                try:
                    await pg.execute('INSERT INTO set_membership (setnum, usernum)'
                                     ' VALUES ($1, $2)', setnum, addable)
                except UniqueViolationError:
                    pass

            await pg.execute('DELETE FROM set_membership WHERE setnum = $1 AND usernum = ANY ($2)',
                             setnum, to_be_deleted)

        await flash('Contact group updated')
        return redirect(f'/sets/contactset/{setnum}')

    # TODO extent the CSRF token to be longer.

    # TODO make the form sustain state across CSRF failure (not a WTForm one this time)
    return await render_template('sets_manage.html', form=form, cset=cset, members=members, nonmembers=nonmembers)
