from functools import wraps
from quart import redirect, url_for, request, g, current_app, session
from wtforms_async import Form
from wtforms_async.csrf.session import SessionCSRF
import arrow
import urllib.parse


def login_required(f):
    @wraps(f)
    async def decorated_function(*args, **kwargs):
        if g.cid is None:
            # return redirect(url_for(f'{current_app.config["XTSSO_ENDPOINT"]}/xtsso/login', return_to=request.url),
            #                status_code=303)
            self_url = current_app.config['SELF_URL_PREFIX'] + request.path
            return redirect(
                current_app.config["XTSSO_ENDPOINT"] + '/sign_in?return_to=' + urllib.parse.quote(self_url,
                                                                                                  safe=''),
                status_code=303)
        return await f(*args, **kwargs)

    return decorated_function


def strip_whitespace(s):
    if s is None:
        return s
    else:
        return s.strip()


class ProtectedForm(Form):
    class Meta:
        csrf = True
        csrf_class = SessionCSRF
        csrf_secret = None
        csrf_context = session


def relativise_time(time):
    return arrow.get(time).humanize()


def in_timezone(time):
    timezone = g.timezone
    return arrow.get(time).to(timezone).format('YYYY-MM-DD HH:mm:ss (ZZ)')


def humane_dateonly(time):
    timezone = g.timezone
    return arrow.get(time).to(timezone).format('DD MMM YYYY')


def lowercase_filter(possibly_str):
    if hasattr(possibly_str, 'lower'):
        return possibly_str.lower()
    return possibly_str
