from quart import Blueprint, request, current_app, g, make_response, render_template, redirect, \
    render_template_string, flash
from . import util
from asyncpg.exceptions import UniqueViolationError

profile_blueprint = Blueprint("profile", __name__)


@profile_blueprint.route('/profile')
@util.login_required
def my_profile():
    # TODO check if first time (there will be a get param?)
    return 'My profile'


@profile_blueprint.route('/users/by-userid/<string:userid>', methods=['GET', 'POST'])
@util.login_required
async def view_profile(userid):
    reqform = await request.form
    form = util.ProtectedForm(reqform)

    async with current_app.pg_pool.acquire() as pg:
        member = await pg.fetchrow('SELECT userid, usernum, display_name, primary_setnum FROM "user" WHERE userid = $1',
                                   userid.lower())

        if member is None:
            await flash('That user does not exist.')
            return redirect('/', status_code=303)

        if request.method == 'POST' and await form.validate():
            if 'req' in reqform:
                # request friends or add to contacts
                try:
                    await pg.execute('INSERT INTO set_membership (setnum, usernum) VALUES ($1, $2)',
                                     g.primary_setnum, member['usernum'])
                except UniqueViolationError:
                    pass  # ignore duplicates
            elif 'rej' in reqform:
                # delete & reject
                # remove the deletee from the user's sets
                await pg.execute('DELETE FROM set_membership WHERE usernum = $2 AND setnum IN '
                                 '(SELECT setnum FROM set WHERE owner_usernum = $1)', g.usernum, member['usernum'])

                # also sweep the user out of the deletee's sets
                await pg.execute('DELETE FROM set_membership WHERE usernum = $2 AND setnum IN '
                                 '(SELECT setnum FROM set WHERE owner_usernum = $1)', member['usernum'], g.usernum)

        is_inbound_contact = await pg.fetchrow('SELECT 1 FROM "set_membership" WHERE setnum=$1 AND usernum=$2',
                                               member['primary_setnum'], g.usernum)
        is_outbound_contact = await pg.fetchrow('SELECT 1 FROM "set_membership" WHERE setnum=$1 AND usernum=$2',
                                                g.primary_setnum, member['usernum'])

        if is_inbound_contact:
            if is_outbound_contact:
                state = "mutual"
            else:
                state = "incoming"
        else:
            if is_outbound_contact:
                state = "outgoing"
            else:
                state = "none"

    return await render_template('profile_view.html', member=member, state=state, form=form)
