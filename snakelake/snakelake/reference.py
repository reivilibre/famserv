from enum import Enum


class ContentTypes(Enum):
    TEXT_PLAIN = 1
    TEXT_MARKDOWN = 2
    GALLERY_ALBUM = 11
