import asyncio
import json
import os
import traceback
import re

import aioredis
import arrow
from quart import Blueprint, request, current_app, render_template, redirect, \
    Response, g

from .reference import *
from . import common, processing, util
from ..prelude import *

BYTES_PER_MIB = 1048576

upload_blueprint = Blueprint("gallery_upload", __name__)


def json_response(content, status=200):
    return Response(
        json.dumps(content, indent=None),
        status=status,
        mimetype='text/plain'
    )


async def complete_upload(uuid, orig_filename, albumnum):
    loop = asyncio.get_event_loop()
    source = common.path_for_uuidimage(current_app, uuid)

    if not (MIN_PHOTO_SIZE <= os.path.getsize(source) <= MAX_PHOTO_SIZE):
        os.unlink(source)
        return json_response({
            'success': False,
            'error': 'Invalid file size.',
            'preventRetry': True,
        }, 400)

    # optimise the image first, as this could affect the hash
    datetime = await loop.run_in_executor(current_app.future_executor, processing.prepare_image,
                                          source, False)

    # now take the hash.
    photohash = await loop.run_in_executor(current_app.future_executor, processing.hash_file, source)

    # print("HASH", photohash)

    target = common.path_for_image(current_app, photohash)
    if os.path.exists(target):
        proc = await asyncio.create_subprocess_exec('cmp', source, target)
        exit_code = await proc.wait()
        if exit_code != 0:
            # fault condition.
            eprint("!!! Difference (blake2s18 collision?) !!!")
            eprint(f"{source} and {target} differ, despite having the same blake2s18 Hash.")
            return json_response({
                'success': False,
                'error': 'blake2s18 collision. Please report this to the administrator.',
                'preventRetry': True,
            }, 500)
        os.unlink(source)

        from PIL import Image
        img = Image.open(target)
        width, height = img.size
        del img
    else:
        os.rename(source, target)

        # now 'postpare' (calculate thumb & medium images)
        width, height = await loop.run_in_executor(current_app.future_executor, processing.postpare_image,
                                                   target)

    if datetime is None:
        timestamp = arrow.utcnow().timestamp
    else:
        timestamp = arrow.get(datetime, 'YYYY:MM:DD HH:mm:ss').timestamp

    # should be stored without extension, and up to 24 chars long
    orig_filename = re.sub(r'\.[^.]*$', '', orig_filename)[0:PHOTO_MAX_RETAINED_FILENAME_LEN]
    filesize = os.path.getsize(target)

    async with current_app.pg_pool.acquire() as pg:
        await pg.execute('INSERT INTO photograph (albumnum, width, height, original_filename, caption, photohash, '
                         'timestamp, filesize) VALUES ($1, $2, $3, $4, \'\', $5, $6, $7)',
                         albumnum, width, height, orig_filename, photohash, timestamp, filesize)

    return json_response({
        'success': True,
    })


@upload_blueprint.route('/<int:albumnum>')
@util.login_required
async def upload(albumnum):
    if not await common.is_users_album(albumnum):
        return redirect('/gallery', status_code=303)
    return await render_template('gallery_upload.html', albumnum=albumnum)


@upload_blueprint.route(
    '/<int:albumnum>/frame')  # partition off the uploader into a frame to prevent CSS leakage
@util.login_required
async def upload_frame(albumnum):
    if not await common.is_users_album(albumnum):
        return Response('404', status=404)
    async with current_app.pg_pool.acquire() as pg:
        usage = await common.get_user_quota_usage(pg, g.usernum)

        quota = "%.1f" % (g.gallery_quota / BYTES_PER_MIB)
        remaining = "%.1f" % ((g.gallery_quota - usage) / BYTES_PER_MIB)
    return await render_template('gallery_uploadframe.html', albumnum=albumnum, remaining=remaining, quota=quota)


@upload_blueprint.route('/handler', methods=['POST'])
@util.login_required
async def upload_handler():
    try:
        form = await request.form
        try:
            uuid = form['qquuid']
            common.assert_valid_uuid(uuid)
            filename = form['qqfilename']
            filesize = int(form['qqtotalfilesize'])
            albumnum = int(form['albumnum'])

            if not await common.is_users_album(albumnum):
                return json_response({
                    'success': False,
                    'error': 'Invalid album.',
                    'preventRetry': True,
                })

            chunknum = None
            numchunks = None

            if 'qqpartindex' in form:
                chunknum = int(form['qqpartindex'])
                numchunks = int(form['qqtotalparts'])

                if filesize / numchunks < MIN_CHUNK_SIZE or chunknum > numchunks:
                    return json_response({
                        'success': False,
                        'error': 'Do not take the Michael.',
                        'preventRetry': True,
                    }, 400)

            if not (MIN_PHOTO_SIZE <= filesize <= MAX_PHOTO_SIZE):
                return json_response({
                    'success': False,
                    'error': 'Invalid presented file size.',
                    'preventRetry': True,
                }, 400)

            files = await request.files
            file = files['qqfile']
        except (KeyError, ValueError):
            return json_response({
                'success': False,
                'error': 'Bad request KV',
            }, 400)

        if chunknum is not None:
            # create an entry in Redis with the metadata

            redis: aioredis.Redis = current_app.redis_pool
            hkey = f"slgUpload:{uuid}"

            hdict = await redis.hgetall(hkey)
            # eprint(f"hdict {uuid} for {chunknum}: {hdict}")

            if not hdict:
                if chunknum <= MAX_INITIAL_CHUNKNUM:
                    hdict = {
                        'filename': filename,
                        'uploaded': file.content_length,
                        'numchunks': numchunks,
                        'albumnum': albumnum
                    }

                    tr = redis.multi_exec()
                    tr.exists(hkey)
                    for k, v in hdict.items():
                        tr.hsetnx(hkey, k, v)
                    result = await tr.execute()

                    if result[0]:
                        # the hashmap already existed so was not written
                        # phew, missed a race there
                        hdict = await redis.hgetall(hkey)
                else:
                    return json_response({
                        'success': False,
                        'error': 'Chunk transmission state inconsistent.'
                    })

            await redis.hincrby(hkey, 'uploaded', file.content_length)

            await redis.expire(hkey, 3600)  # each chunk can take 1 hour max

            # check the user's quota — every chunk to prevent an exploit, though I'd rather not cancel
            # an upload mid-way. TODO Check & commit upfront (in Redis, for example, maybe using the list type with
            # item expiry if that exists?)
            async with current_app.pg_pool.acquire() as pg:
                current_usage = await common.get_user_quota_usage(pg, g.usernum)

            if current_usage + filesize > g.gallery_quota:
                return json_response({
                    'success': False,
                    'error': 'You have exceeded your quota. Please ask the administrator if you want more.',
                    'preventRetry': True
                })

            if int(hdict['numchunks']) != numchunks:
                return json_response({
                    'success': False,
                    'error': 'Inconsistent numchunks.',
                    'preventRetry': True,
                }, 400)

            if int(hdict['uploaded']) > MAX_PHOTO_SIZE:
                return json_response({
                    'success': False,
                    'error': 'Invalid file size.',
                    'preventRetry': True,
                }, 400)

            save_to = common.path_for_uuidchunk(current_app, uuid, chunknum)
            os.makedirs(os.path.dirname(save_to), mode=0o770, exist_ok=True)
            file.save(save_to)
            return json_response({
                'success': True
            }, 200)
        else:
            async with current_app.pg_pool.acquire() as pg:
                current_usage = await common.get_user_quota_usage(pg, g.usernum)
            if current_usage + filesize > g.gallery_quota:
                return json_response({
                    'success': False,
                    'error': 'You have exceeded your quota. Please ask the administrator if you want more.',
                    'preventRetry': True
                })
            # move straight to the destination and complete the upload.
            save_to = common.path_for_uuidimage(current_app, uuid)
            os.makedirs(os.path.dirname(save_to), mode=0o770, exist_ok=True)
            file.save(save_to)
            return await complete_upload(uuid, filename, albumnum)
    except Exception:
        traceback.print_exc()
        return json_response({
            'success': False,
            'error': 'Internal server error'
        }, 500)


@upload_blueprint.route('/chunksdone', methods=['POST'])
@util.login_required
async def upload_chunksdone():
    form = await request.form
    try:
        uuid = form['qquuid']
        common.assert_valid_uuid(uuid)
    except (KeyError, ValueError):
        return json_response({
            'success': False,
            'error': 'Bad request',
        }, 400)

    # for testing clean-up, uncomment this fake success so you get residue left behind:
    # return json_response({'success': True}, 200)

    redis: aioredis.Redis = current_app.redis_pool
    hkey = f"slgUpload:{uuid}"

    tr = redis.multi_exec()
    tr.hgetall(hkey)
    tr.delete(hkey)
    hdict = (await tr.execute())[0]

    if hdict is None:
        return json_response({
            'success': False,
            'error': 'File upload does not exist',
        }, 400)

    # stitch the file together.
    save_to = common.path_for_uuidimage(current_app, uuid)
    os.makedirs(os.path.dirname(save_to), mode=0o770, exist_ok=True)

    try:
        numchunks = int(hdict['numchunks'])
    except KeyError:
        return json_response({
            'success': False,
            'error': f'For {uuid}, hdict was {hdict} and no numchunks.',
            'reset': True,
        }, 500)
    try:
        with open(save_to, 'wb') as fout:
            for chunknum in range(numchunks):
                chunkfilename = common.path_for_uuidchunk(current_app, uuid, chunknum)
                with open(chunkfilename, 'rb') as fin:
                    while True:
                        buf = fin.read(32768)
                        if buf == b'':
                            break
                        fout.write(buf)
                os.unlink(chunkfilename)
    except IOError:
        traceback.print_exc()
        return json_response({
            'success': False,
            'error': 'Failed to stitch photograph. Contact administrator if problem persists.',
            'preventRetry': True,
        }, 500)

    return await complete_upload(uuid, hdict['filename'], int(hdict['albumnum']))

# possibly for the future, but for now I don't care too much.
# @gallery_blueprint.route('/upload/handler/<uuid>', methods=['DELETE'])
# async def upload_cancellation(uuid):
#     appdir = current_app.config['PHOTO_UPLOAD_DIR']
#
#     pass
