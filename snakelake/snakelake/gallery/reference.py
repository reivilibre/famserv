GOLD_THUMBNAIL_SIZE = (400, 248)
# tested by eye
GOLD_THUMBNAIL_QUALITY = 42
MEDIUM_QUALITY = 80
MEDIUM_MAX_SIZE = (1920, 1080)
MIN_PHOTO_SIZE = 8192
MAX_PHOTO_SIZE = 42 * 1024 * 1024
# any average chunk size less than this will be considered as 'taking the Michael'.
MIN_CHUNK_SIZE = 512
# Chunks may sometimes arrive out of order, this specifies the maximum first received chunknum that will be accepted
MAX_INITIAL_CHUNKNUM = 5

# copied from schema.
GALLERY_UNLISTED_RAW_BYTES_LENGTH = 9  # 3/4 of base64
GALLERY_UNLISTED_BASE64_LENGTH = 12
PHOTO_MAX_RETAINED_FILENAME_LEN = 24
