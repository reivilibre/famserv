import secrets

from quart import Blueprint, render_template, request, redirect
from wtforms_async import StringField
from wtforms_async.validators import Length

from . import processing
from .common import *
from .reference import *
from .. import util

gallery_blueprint = Blueprint("gallery", __name__)


class AlbumForm(util.ProtectedForm):
    name = StringField('Name of the album:', filters=[util.strip_whitespace],
                       validators=[Length(min=1, max=32)])


@gallery_blueprint.route('/')
@util.login_required
async def root():
    async with current_app.pg_pool.acquire() as pg:
        albums = await pg.fetch('SELECT name, albumnum FROM album'
                                ' WHERE owner_usernum=$1 AND parent_albumnum IS NULL'
                                ' ORDER BY albumnum DESC',
                                g.usernum)  # TODO include cover photo
    return await render_template('gallery_gallery.html', albums=albums, nav_active='mine')


@gallery_blueprint.route('/<int:albumnum>/share')
@util.login_required
async def share_album(albumnum):
    async with current_app.pg_pool.acquire() as pg:
        album = await pg.fetchrow('SELECT name, albumnum, unlisted_token FROM album'
                                  ' WHERE owner_usernum=$1 AND albumnum=$2',
                                  g.usernum, albumnum)
        if album is None:
            return redirect('/gallery', status_code=303)

    return await render_template('gallery_share.html', album=album, nav_active='mine')


@gallery_blueprint.route('/<int:albumnum>')
@util.login_required
async def view_album(albumnum):
    async with current_app.pg_pool.acquire() as pg:
        album = await pg.fetchrow('SELECT name, albumnum, unlisted_token FROM album'
                                  ' WHERE owner_usernum=$1 AND albumnum=$2',
                                  g.usernum, albumnum)
        if album is None:
            return redirect('/gallery', status_code=303)

        # subalbums = await pg.fetch('SELECT name, albumnum ') TODO want LJ the cover photo at the same time.
        subalbums = []  # TODO

        photos = await pg.fetch('SELECT photonum, width, height, photohash, timestamp FROM photograph'
                                ' WHERE albumnum=$1'
                                ' ORDER BY timestamp DESC', album['albumnum'])

    return await render_template('gallery_gallery.html', this_album=album, nav_active='mine', back_up='/gallery/',
                                 photos=photos)


@gallery_blueprint.route('/<int:albumnum>:<string:unlisted_token>')
async def view_shared_album(albumnum, unlisted_token):
    async with current_app.pg_pool.acquire() as pg:
        album = await pg.fetchrow('SELECT name, albumnum, unlisted_token FROM album'
                                  ' WHERE unlisted_token=$1 AND albumnum=$2',
                                  unlisted_token, albumnum)
        if album is None:
            return redirect('/gallery', status_code=303)

        # subalbums = await pg.fetch('SELECT name, albumnum ') TODO want LJ the cover photo at the same time.
        subalbums = []  # TODO

        photos = await pg.fetch('SELECT photonum, width, height, photohash, timestamp FROM photograph'
                                ' WHERE albumnum=$1'
                                ' ORDER BY timestamp DESC', album['albumnum'])

    return await render_template('gallery_gallery.html', this_album=album, photos=photos)


@gallery_blueprint.route('/<int:parent_albumnum>/new', methods=['GET', 'POST'])
@util.login_required
async def create_child_album(parent_albumnum):
    # todo select parent
    parent = 42
    form = AlbumForm(await request.form)
    return await render_template('gallery_new_album.html', parent=parent, form=form)


@gallery_blueprint.route('/new', methods=['GET', 'POST'])
@util.login_required
async def create_toplevel_album():
    form = AlbumForm(await request.form)
    if request.method == 'POST' and await form.validate():
        unlisted_token = secrets.token_urlsafe(GALLERY_UNLISTED_RAW_BYTES_LENGTH)

        async with current_app.pg_pool.acquire() as pg:
            insertrow = await pg.fetchrow('INSERT INTO album (parent_albumnum, name, unlisted_token, owner_usernum)'
                                          ' VALUES (NULL, $1, $2, $3)'
                                          ' RETURNING albumnum',
                                          form.name.data, unlisted_token, g.usernum)
            return redirect(f'/gallery/{insertrow[0]}', status_code=303)
    return await render_template('gallery_new_album.html', parent=None, form=form)
