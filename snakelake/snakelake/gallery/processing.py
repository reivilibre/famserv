import asyncio
import base64
import hashlib
import os
import subprocess
import traceback
import aioredis

from . import common
from .reference import *


async def scheduled_cancel_old(app):
    """
    Clean up expired/timed-out chunks.
    """
    await asyncio.sleep(app.config['PHOTO_CLEANUP_DELAY_INITIAL'])
    while True:
        try:
            redis: aioredis.Redis = app.redis_pool
            chunkdir = app.config['PHOTO_UPLOAD_DIR'] + '/chunks'
            for filename in os.listdir(chunkdir):
                # chunk filenames look like UUID.1.jpg
                unextended, extension = os.path.splitext(filename)
                chunkuuid, chunknum = os.path.splitext(unextended)
                if common.naive_is_uuid(chunkuuid):
                    if not await redis.exists(f"slgUpload:{chunkuuid}"):
                        os.unlink(chunkdir + '/' + filename)
        except:
            traceback.print_exc()
        await asyncio.sleep(app.config['PHOTO_CLEANUP_INTERVAL'])


def hash_file(filename):
    """
    Hashes the file with the blake2s(18) message digest algorithm
    """
    hasher = hashlib.blake2s(digest_size=18, person=b'SNAKElac')
    with open(filename, 'rb') as fin:
        while True:
            buf = fin.read(512)
            if buf == b'':
                break
            hasher.update(buf)
    return base64.urlsafe_b64encode(hasher.digest()).decode()  # should yield 24 characters


def postpare_image(filename):
    from PIL import Image
    # we want a fixed-width thumbnail of 400px

    img = Image.open(filename)
    cur_width, cur_height = img.size

    desired_ratio = GOLD_THUMBNAIL_SIZE[0] / GOLD_THUMBNAIL_SIZE[1]
    actual_ratio = cur_width / cur_height
    if desired_ratio > actual_ratio:
        # image is too tall
        new_height = int((GOLD_THUMBNAIL_SIZE[0] / cur_width) * cur_height)
        transitional = img.resize((GOLD_THUMBNAIL_SIZE[0], new_height), Image.LANCZOS)
        excess_height = new_height - GOLD_THUMBNAIL_SIZE[1]
        trim_from_bottom = excess_height // 2
        trim_from_top = excess_height - trim_from_bottom
        thumb = transitional.crop((0, trim_from_top, GOLD_THUMBNAIL_SIZE[0], new_height - trim_from_bottom))
    else:
        # image is too wide
        new_width = int((GOLD_THUMBNAIL_SIZE[1] / cur_height) * cur_width)
        transitional = img.resize((new_width, GOLD_THUMBNAIL_SIZE[1]), Image.LANCZOS)
        excess_width = new_width - GOLD_THUMBNAIL_SIZE[0]
        trim_from_left = excess_width // 2
        trim_from_right = excess_width - trim_from_left
        thumb = transitional.crop((trim_from_left, 0, new_width - trim_from_left, GOLD_THUMBNAIL_SIZE[1]))

    unextended_name = os.path.splitext(filename)[0]
    # according to https://yuiblog.com/blog/2008/12/05/imageopt-4/
    # progressive should be True for larger than 10kiB, but thumbnails are unlikely to be that.
    thumb.save(unextended_name + '.t.jpg', optimize=True, progressive=False, quality=GOLD_THUMBNAIL_QUALITY)
    del thumb

    # resize img as medium

    max_width, max_height = MEDIUM_MAX_SIZE

    if cur_width > max_width or cur_height > max_height:
        medium_filename = unextended_name + '.m.jpg'

        if (cur_width / max_width) > (cur_height / max_height):
            new_width = max_width
            new_height = int((max_width / cur_width) * cur_height)
        else:
            new_height = max_height
            new_width = int((max_height / cur_height) * cur_width)

        img = img.resize((new_width, new_height), Image.LANCZOS)
        img.save(medium_filename, optimize=True, progressive=True, quality=MEDIUM_QUALITY)

        # optimise the medium image losslessly
        # shouldn't be required assuming Pillow is competitive.
        # subprocess.check_call(['jpegtran', '-optimise', '-progressive', '-copy', 'none', medium_filename])
    del img

    return cur_width, cur_height


def prepare_image(filename, preserve_exif):
    import piexif

    try:
        exif_data = piexif.load(filename)
    except:
        exif_data = {'0th': {}}
    # save a small amount of space by removing the built-in thumbnail
    if 'thumbnail' in exif_data:
        exif_data.pop('thumbnail')
    if not preserve_exif:
        piexif.remove(filename)

    try:
        orientation = exif_data['0th'][piexif.ImageIFD.Orientation]
        transformation = None
        if orientation == 2:
            transformation = ['-flip', 'horizontal']
        elif orientation == 3:
            transformation = ['-rotate', '180']
        elif orientation == 4:
            transformation = ['-flip', 'vertical']
        elif orientation == 5:
            transformation = ['-transpose']
        elif orientation == 6:
            transformation = ['-rotate', '90']
        elif orientation == 7:
            transformation = ['-transverse']
        elif orientation == 8:
            transformation = ['-rotate', '270']

        if preserve_exif:
            exif_data['0th'][piexif.ImageIFD.Orientation] = 1  # reset to upright.

        if transformation is not None:
            # NOT strictly lossless — quality is the same but the image will be trimmed to nearest 8 or 16 px
            with open(filename + '.tmp', 'w') as fout_tmp:
                subprocess.check_call(['jpegtran', '-trim', '-copy', 'none'] + transformation + [filename],
                                      stdout=fout_tmp)
            os.unlink(filename)
            os.rename(filename + '.tmp', filename)
    except KeyError:
        pass

    with open(filename + '.tmp', 'w') as fout_tmp:
        # optimise the user's image losslessly
        subprocess.check_call(['jpegtran', '-optimise', '-progressive', '-copy', 'none', filename],
                              stdout=fout_tmp)
    os.unlink(filename)
    os.rename(filename + '.tmp', filename)

    try:
        datetime = exif_data['0th'][piexif.ImageIFD.DateTime].decode('utf-8')
    except KeyError:
        datetime = None

    if preserve_exif:
        piexif.insert(exif_data, filename)

    return datetime
