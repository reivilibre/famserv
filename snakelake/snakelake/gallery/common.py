import re

from quart import current_app, g


async def is_users_album(albumnum):
    if g.cid is None:
        return False
    async with current_app.pg_pool.acquire() as pg:
        result = await pg.fetchrow("SELECT 1 FROM album WHERE albumnum = $1 AND owner_usernum = $2",
                                   albumnum, g.usernum)
        return result is not None


def path_for_image(app, blake2s18_urlsafeb64):
    return app.config['PHOTO_UPLOAD_DIR'] + '/' + blake2s18_urlsafeb64 + '.jpg'


def path_for_uuidimage(app, uuid):
    return app.config['PHOTO_UPLOAD_DIR'] + '/' + uuid + '.jpg'


def path_for_uuidchunk(app, uuid, chunk_num):
    return app.config['PHOTO_UPLOAD_DIR'] + '/chunks/' + uuid + '.' + str(chunk_num) + '.jpg'


def naive_is_uuid(uuid):
    # naïve and perhaps wrong, but it shouldn't cause any unsafety.
    # The main concern is to prevent null bytes and slashes.
    return bool(re.match('^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$', uuid))


def assert_valid_uuid(uuid):
    # naïve and perhaps wrong, but it shouldn't cause any unsafety.
    # The main concern is to prevent null bytes and slashes.
    if not naive_is_uuid(uuid):
        raise ValueError("Not a valid UUID")


async def get_user_quota_usage(pg, usernum):
    """
    Calculates the user's quota usage. Duplicate photos are not counted.
    Remember that the quota is EXCLUSIVE of thumbnails & mediums — so expect a ~20% inflation on disk usage
    compared to what this shows.
    """
    result = await pg.fetchrow("""
        SELECT SUM(filesize) FROM
            (SELECT DISTINCT p.photohash, p.filesize
             FROM photograph p, album a 
             WHERE p.albumnum = a.albumnum
             AND a.owner_usernum = $1) AS usage
    """, usernum)
    if result is None:
        return 0
    elif result[0] is None:
        return 0
    else:
        return result[0]
