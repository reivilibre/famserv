from jinja2 import evalcontextfilter, Markup, escape
from .reference import ContentTypes
import re

URL_PATTERN = re.compile(r'(http|https|ftp|ftps)://[a-zA-Z0-9./_%?&#=:;-]+')


@evalcontextfilter
def paragraphise(eval_ctx, value):
    result = '\n'.join('<p>%s</p>' % p.replace('\n', Markup('<br>\n'))
                       for p in escape(value).split('\n\n'))
    if eval_ctx.autoescape:
        result = Markup(result)
    return result


@evalcontextfilter
def hyperlinks_and_paragraphs(eval_ctx, value):
    """
    Trivial 'formatting' that simply converts paragraphs and hyperlinks.
    """

    # print("hap", eval_ctx, value)

    def hyperlinkise(text):
        def hyperlinkify(match):
            escaped = match.group(0)  # pre-escaped in `for p in ...`
            return Markup(f'<a href="{escaped}">{escaped}</a>')

        return URL_PATTERN.sub(hyperlinkify, text)

    # TODO what is the significance of Markup(<br>\n)?
    result = '\n'.join('<p>%s</p>' % hyperlinkise(p).replace('\n', Markup('<br>\n'))
                       for p in escape(value).split('\n\n'))
    if eval_ctx.autoescape:
        result = Markup(result)
    return result


@evalcontextfilter
def specified_formatting(eval_ctx, input, specified: ContentTypes or int):
    if isinstance(specified, int):
        specified = ContentTypes(specified)
    if specified == ContentTypes.TEXT_PLAIN:
        return hyperlinks_and_paragraphs(eval_ctx, input)
    elif specified == ContentTypes.TEXT_MARKDOWN:
        raise NotImplementedError
    elif specified == ContentTypes.GALLERY_ALBUM:
        raise NotImplementedError
    else:
        print(f"Unknown type {specified}")
