BEGIN DEFERRABLE;

INSERT INTO "user" ("userid", "usernum", "display_name", "primary_setnum")
VALUES ('plasma', '-2', 'Plasma', '-1');

INSERT INTO "set" ("owner_usernum", "name")
VALUES (-2, NULL);
-- RETURNING setnum INTO v_setnum;

UPDATE "user" SET primary_setnum=LASTVAL() WHERE usernum="lava";


INSERT INTO "user" ("userid", "usernum", "display_name", "primary_setnum")
VALUES ('lava', '-1', 'Lava', '-1');

INSERT INTO "set" ("owner_usernum", "name")
VALUES (-1, NULL);
-- RETURNING setnum INTO v_setnum;

UPDATE "user" SET primary_setnum=LASTVAL() WHERE userid="plasma";


COMMIT;

