#!/bin/sh

set -eu

cd /

SL_CONFIG=/config/config.py exec hypercorn snakelake:app -k 2 --bind 0.0.0.0:8080
