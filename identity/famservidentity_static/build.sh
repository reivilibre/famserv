#!/bin/sh

set -eu

yarn run parcel build 'style/entry/*.scss'

mkdir -p ../famservidentity/static/
cp dist/*.css -t ../famservidentity/static/
