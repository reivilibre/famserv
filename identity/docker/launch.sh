#!/bin/sh

set -eu

cd /

FSI_CONFIG=/config/config.py exec hypercorn famservidentity:app -k 2 -b 0.0.0.0:8080
