#!/bin/sh

set -eu

apk add --no-cache python3 git libffi libffi-dev python3-dev g++ make linux-headers

pip3 install git+https://gitlab.com/reivilibre/wtforms_async.git

# install dependencies only
pip3 install -r /requirements.txt

pip3 install hypercorn

adduser -H -D -s /sbin/nologin identity

mkdir -p /config

mv /example_config.py /config/config.py

chown -R identity:identity /famservidentity /config

rm /setup.sh /requirements.txt
