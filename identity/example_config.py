REDIS_ADDRESS = 'redis://127.0.0.1'
REDIS_DB = 0
REDIS_PASSWORD = ''
REDIS_POOL_MAX_SIZE = 2
REDIS_EXPIRY = 7 * 86400  # 7 days

PG_HOST = '127.0.0.1'  # or socket.
PG_USER = ''
PG_PASSWORD = ''
PG_DATABASE = ''

SECRET_KEY = ''

DIRECT_INVITEES_TO = 'example.org'
SERVICE_URLS = {
    'friendica': 'https://friendica.example.org',
    'peertube': 'https://peertube.example.org'
}

RESERVED_USERIDS = {
    'admin',
}

TITLE = 'SiteName'
SUBTITLE = 'SiteSubtitle'
COPY_OPERATORS = 'The SiteOperator(s)'
COPY_YEAR = 1970

# IP_ADDRESS_HEADER = 'X-Forwarded-For'
IP_ADDRESS_HEADER = None

INTERNAL_ERROR_ADVICE = 'Please contact the operator via e-mail: xyz@xyz.xyz'
CONTACT_PAGE_INFO = 'xyz [@] xyz [.] xyz'

COOKIE_KEY = 'xtsso'
COOKIE_MAXAGE = 7 * 86400  # 7 days
COOKIE_PATH = '/'
COOKIE_DOMAIN = '.example.org'
COOKIE_SECURE = True
COOKIE_HTTPONLY = True

INVITATION_EXPIRY = 7 * 86400
ACTIVE_INVITATIONS_PER_INVITER_LIMIT = 5
AT_DOMAIN = 'example.org'

RATELIMIT_PERIOD = 600  # 600 seconds = 10 minutes
RATELIMIT_ATTEMPTS = 10  # 10 attempts per 10 minutes.
