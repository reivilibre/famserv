import asyncio
from functools import wraps
from quart import redirect, url_for, request, g, current_app
from datetime import datetime


async def rate_limited_action(req, cfg, redis):
    user_ip = get_ip_address(req)

    attempt_number = await redis.incr(f'rateLimiting:{user_ip}')
    if attempt_number == 1:
        await redis.expire(f'rateLimiting:{user_ip}', cfg['RATELIMIT_PERIOD'])

    return attempt_number <= cfg['RATELIMIT_ATTEMPTS']


async def check_login():
    redis = current_app.redis_pool

    sess_key = request.cookies.get(current_app.config['COOKIE_KEY'])

    if sess_key is not None:
        # check session key in Redis
        session_dbkey = f'session:{sess_key}'
        canonical_userid = await redis.hget(session_dbkey, 'cid')
        if canonical_userid is not None:
            g.cid = canonical_userid
            return True
    return False


def login_required(f):
    @wraps(f)
    async def decorated_function(*args, **kwargs):
        await check_login()
        if g.cid is None:
            return redirect(url_for('/xtsso/login', return_to=request.url), status_code=303)
        return await f(*args, **kwargs)

    return decorated_function


def relative_future_time(future_time):
    now = int(datetime.utcnow().timestamp())
    duration = future_time - now
    return relative_future_duration(duration)


def relative_future_duration(duration):
    first_part, rem = relative_duration_part(duration)
    if rem == 0:
        return first_part
    second_part, _ = relative_duration_part(rem)
    return f'{first_part} {second_part}'


def relative_duration_part(duration):
    days, rem = divmod(duration, 86400)
    if days > 0:
        return f'{days} days', rem
    hours, rem = divmod(duration, 3600)
    if hours > 0:
        return f'{hours} hours', rem
    minutes, rem = divmod(duration, 60)
    if minutes > 0:
        return f'{minutes} minutes', rem
    return f'{duration} seconds', rem


def lowercase_filter(possibly_str):
    if hasattr(possibly_str, 'lower'):
        return possibly_str.lower()
    return possibly_str


def get_ip_address(req):
    header_name = current_app.config['IP_ADDRESS_HEADER']
    if header_name is None:
        return req.remote_addr
    else:
        if header_name in req.headers:
            return req.headers[header_name]
        else:
            return req.remote_addr
