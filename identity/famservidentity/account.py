from quart import Blueprint, request, current_app, g, make_response, render_template, redirect, \
    render_template_string, flash
from quart.wrappers import Response
from wtforms_async import Form, StringField, PasswordField
from wtforms_async.validators import InputRequired, Length, EqualTo, ValidationError, Regexp
import aioredis
import secrets
from .util import login_required
from passlib.hash import bcrypt
import re
from datetime import datetime
from . import util
import os.path

INVITATION_KEY_REGEX = r"^\s*([a-zA-Z]+)\s*([0-9]+)\s*([a-zA-Z]+)\s*$"

account_blueprint = Blueprint("account", __name__)


async def user_id_not_taken(_form, field):
    if field in current_app.config['RESERVED_USERIDS']:
        raise ValidationError("That user ID is reserved.")

    row = await g.pg.fetchrow("""
        SELECT 1 FROM users WHERE userid = $1
    """, field.data)
    if row is not None:
        raise ValidationError("That user ID is already taken.")


async def valid_key_validator(_form, field):
    if len(field.errors) == 0:
        timestamp = datetime.utcnow().timestamp()

        key = transform_key(field.data)

        print(f'SELECT 1 FROM invitation_keys WHERE key = {key} AND expiry_timestamp > {timestamp}')

        result = await g.pg.fetchrow('SELECT 1 FROM invitation_keys WHERE key = $1 AND expiry_timestamp > $2'
                                     , key, timestamp)

        if result is None:
            raise ValidationError(
                "That invitation code is not valid. Is it spelt correctly? Maybe it expired?")


class RegistrationForm(Form):
    invitation_code = StringField('Invitation Code',
                                  [InputRequired(),
                                   Regexp(INVITATION_KEY_REGEX,
                                          message="That does not look like an invitation code;"
                                                  " it should be 2 words separated by a number."),
                                   valid_key_validator])
    userid = StringField('Desired User ID',
                         [InputRequired(), user_id_not_taken,
                          Regexp(r"^[a-z].*$", message="Your User ID must start with a letter"),
                          Regexp(r"^.*[a-z0-9]$", message="Your User ID must end with a letter or number"),
                          Regexp(r"^[a-z][a-z0-9_]*$",
                                 message="Your User ID must only contain letters, numerals and/or _."),
                          Length(min=3, max=20)],  # TODO is 20 reasonable and sane lowest common?
                         filters=[util.lowercase_filter])
    passphrase = PasswordField('Passphrase', [InputRequired()])
    passphrase_again = PasswordField('Passphrase (again)', [InputRequired(), Length(min=8, max=1024),
                                                            EqualTo('passphrase_again',
                                                                    message='The passphrases do not match.')])


def transform_key(key_text):
    match = re.match(INVITATION_KEY_REGEX, key_text)

    if match is None:
        raise ValueError(
            "That doesn't look like an invitation key.")

    return f"{match.group(1)[0:3]}{match.group(2)}{match.group(3)[0:3]}".lower()


async def generate_invitation_code():
    pg = g.pg
    with open(os.path.join(os.path.dirname(__file__), 'mnemonic_wordlist.txt')) as f:
        wordlist = [ln.strip() for ln in f if len(ln.strip()) > 0]

    async with pg.transaction():
        count = await pg.fetchrow('SELECT COUNT(1) FROM invitation_keys WHERE creator = $1', g.cid)

        if count[0] > current_app.config['ACTIVE_INVITATIONS_PER_INVITER_LIMIT']:
            # return None, "You already have the maximum number allowed."
            # we don't need to display an error as it will show that the limit is saturated
            # at the bottom anyway.
            return None, None

        for iteration in range(256):
            word1idx = secrets.randbelow(len(wordlist))
            word2idx = secrets.randbelow(len(wordlist))
            middle_number = secrets.randbelow(100)

            timestamp_now = datetime.utcnow().timestamp()
            timestamp_expire = timestamp_now + current_app.config['INVITATION_EXPIRY']

            key_human = f"{wordlist[word1idx].title()} {middle_number} {wordlist[word2idx].title()}"
            key_db = transform_key(key_human)

            result = await pg.fetchrow('SELECT 1 FROM invitation_keys WHERE key = $1 AND expiry_timestamp > $2',
                                       key_db, timestamp_now)

            if result is None:
                # invitation key not already in use.
                await pg.execute(
                    'INSERT INTO invitation_keys '
                    '(key, key_human, creator, expiry_timestamp)'
                    ' VALUES ($1, $2, $3, $4)',
                    key_db, key_human, g.cid, timestamp_expire)

                return key_human, None
        else:
            return None, "Unable to generate new invitation code. (exhausted)"


@account_blueprint.route('/invite', methods=['GET', 'POST'])
@login_required
async def invite():
    async with current_app.pg_pool.acquire() as g.pg:
        pg = g.pg
        timestamp_now = datetime.utcnow().timestamp()
        await pg.execute('DELETE FROM invitation_keys WHERE expiry_timestamp < $1',
                         timestamp_now)

        if request.method == 'POST':
            new_invitation_code, new_invitation_error = await generate_invitation_code()
        else:
            new_invitation_code, new_invitation_error = None, None

        existing_invitation_codes = await pg.fetch('SELECT key_human, expiry_timestamp FROM invitation_keys'
                                                   ' WHERE expiry_timestamp > $1 AND creator = $2'
                                                   ' ORDER BY expiry_timestamp', timestamp_now, g.cid)

        can_issue_new = len(existing_invitation_codes) < current_app.config['ACTIVE_INVITATIONS_PER_INVITER_LIMIT']

        return await render_template('account_invite.html', can_issue_new=can_issue_new,
                                     pending_invitations=existing_invitation_codes, new_code=new_invitation_code,
                                     new_code_error=new_invitation_error)


async def registration_handler(form):
    """
    The actual handler for sign-in.
    """
    cfg = current_app.config

    if not await util.rate_limited_action(request, current_app.config, current_app.redis_pool):
        form.userid.errors.append(
            "You have made too many registration attempts in a short period of time. Please try again later.")
        return

    # register the account
    hashed_password = '{BLF-CRYPT}' + bcrypt.hash(form.passphrase.data)

    async with g.pg.transaction():
        await g.pg.execute("""
            DELETE FROM invitation_keys WHERE key = $1
        """, transform_key(form.invitation_code.data))

        await g.pg.execute("""
            INSERT INTO users
            (userid, passhash)
            VALUES ($1, $2)
        """, form.userid.data, hashed_password)

        await flash('You have now successfully registered and you may sign in.')
        return redirect('/xtsso/sign_in', status_code=303)


@account_blueprint.route('/register', methods=['GET', 'POST'])
async def registration():
    form = RegistrationForm(await request.form)

    if request.method == 'POST':
        async with current_app.pg_pool.acquire() as g.pg:
            # pg is required for form validation.
            # TODO should the invitation code be a post-validation (after the rate limiting?) YES.
            if await form.validate():
                handled = await registration_handler(form)
                if handled is not None:
                    return handled

    with open(os.path.join(os.path.dirname(__file__), 'eff-and-rei-reasonablylong-english.txt')) as f:
        wordlist = [ln.strip() for ln in f if len(ln.strip()) > 0]

    def generate_passphrase(numwords=4):
        pieces = [wordlist[secrets.randbelow(len(wordlist))] for _ in range(numwords)]
        return ' '.join(pieces)

    passphrase_suggestions = [generate_passphrase() for _ in range(5)]

    return await render_template('account_register.html', form=form, passphrase_suggestions=passphrase_suggestions)
