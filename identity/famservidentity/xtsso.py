from quart import Blueprint, request, current_app, g, make_response, render_template, redirect
from wtforms_async import Form, StringField, PasswordField
from wtforms_async.validators import InputRequired
import aioredis
import secrets
from . import util

xtsso_blueprint = Blueprint("xtsso", __name__)


class SignInForm(Form):
    userid = StringField('User ID', [InputRequired()], filters=[util.lowercase_filter])
    passphrase = PasswordField('Passphrase', [InputRequired()])


async def sign_in_handler(form, return_to):
    """
    The actual handler for sign-in.
    """
    redis: aioredis.Redis = current_app.redis_pool
    cfg = current_app.config

    if not await util.rate_limited_action(request, cfg, redis):
        form.userid.errors.append(
            "You have made too many sign-in attempts in a short period of time. Please try again later.")
        return

    async with current_app.pg_pool.acquire() as pg:
        user_row = await pg.fetchrow("""
                        SELECT userid, passhash FROM users WHERE userid=$1
                    """, form.userid.data)

        if user_row is None:
            form.userid.errors.append("That userid does not exist.")
            return

    display_name = canonical_userid = user_row['userid']

    # display_name = 'Stephen P. Q. Rotherham' idea for later

    # redirect to final page via 303 See Other (downgrades to a GET request by definition)
    resp = await make_response(redirect(return_to or '/', status_code=303))

    # create session key
    sess_key = secrets.token_urlsafe(16)

    resp.set_cookie(cfg['COOKIE_KEY'], sess_key, max_age=cfg['COOKIE_MAXAGE'], path=cfg['COOKIE_PATH'],
                    domain=cfg['COOKIE_DOMAIN'], secure=cfg['COOKIE_SECURE'], httponly=cfg['COOKIE_HTTPONLY'])

    redis_key = f'session:{sess_key}'
    await redis.hmset_dict(redis_key, {
        'cid': canonical_userid,
        'dnm': display_name
    })
    await redis.expire(redis_key, cfg['REDIS_EXPIRY'])

    set_redis_key = f'sessionsFor:{canonical_userid}'
    await redis.sadd(set_redis_key, sess_key)
    await redis.expire(set_redis_key, cfg['REDIS_EXPIRY'])

    # clean up the sessionsFor set of expired sessions
    to_remove = []
    async for other_sess_key in redis.isscan(set_redis_key):
        redis_key = f'session:{other_sess_key}'
        if not await redis.exists(redis_key):
            to_remove.append(other_sess_key)

    if len(to_remove) > 0:
        await redis.srem(set_redis_key, *to_remove)

    return resp


@xtsso_blueprint.route('/sign_in', methods=['GET', 'POST'])
async def sign_in():
    return_to = request.args.get('return_to')

    form = SignInForm(await request.form)
    if request.method == 'POST' and await form.validate():
        handled = await sign_in_handler(form, return_to)
        if handled is not None:
            return handled

    return await render_template('xtsso_signin.html', form=form)


@xtsso_blueprint.route('/sign_out', methods=['GET', 'POST'])
async def sign_out():
    cfg = current_app.config
    sess_key = request.cookies.get(cfg['COOKIE_KEY'])
    if request.method == 'POST':
        resp = await make_response(render_template('xtsso_signout.html', done=True))
        if sess_key is not None:
            redis: aioredis.Redis = current_app.redis_pool

            session_dbkey = f'session:{sess_key}'

            canonical_userid = await redis.hget(session_dbkey, 'cid')

            await redis.delete(session_dbkey)
            await redis.srem(f'sessionsFor:{canonical_userid}', session_dbkey)

            resp.set_cookie(cfg['COOKIE_KEY'], '', max_age=-42, path=cfg['COOKIE_PATH'], domain=cfg['COOKIE_DOMAIN'],
                            secure=cfg['COOKIE_SECURE'], httponly=cfg['COOKIE_HTTPONLY'])

        return resp

    return await render_template('xtsso_signout.html', done=sess_key is None)
