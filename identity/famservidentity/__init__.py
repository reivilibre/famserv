from quart import Quart, g, render_template, current_app, request
from quart.wrappers import Response
# from asyncio import queues
import asyncio
import aioredis
import asyncpg
from . import xtsso, account
from . import util

loop = asyncio.get_event_loop()

app = Quart(__name__)

app.config.from_envvar('FSI_CONFIG')

app.register_blueprint(xtsso.xtsso_blueprint, '/xtsso')
app.register_blueprint(account.account_blueprint, '/account')


# create a redis connection
# this adds it to the first layer of g, which will not be lost between requests
# changes to g during a request are not persisted between requests, though.
async def register_pools():
    async with app.app_context():
        cfg = app.config
        current_app.redis_pool = await aioredis.create_redis_pool(cfg['REDIS_ADDRESS'], db=cfg['REDIS_DB'],
                                                                  password=cfg['REDIS_PASSWORD'], minsize=1,
                                                                  maxsize=cfg['REDIS_POOL_MAX_SIZE'],
                                                                  encoding='utf-8')
        current_app.pg_pool = await asyncpg.create_pool(host=cfg['PG_HOST'], database=cfg['PG_DATABASE'],
                                                        user=cfg['PG_USER'],
                                                        password=cfg['PG_PASSWORD'])

        current_app.jinja_env.filters['relative_future_duration'] = util.relative_future_duration
        current_app.jinja_env.filters['relative_future_time'] = util.relative_future_time


loop.run_until_complete(register_pools())


@app.route('/about/privacy')
async def privacy():
    return await render_template('index_privacy.html')


@app.route('/about/credits')
async def attributions():
    return await render_template('index_credits.html')


@app.route('/about/contact')
async def contact():
    return await render_template('index_contact.html')


@app.route('/')
async def root_index():
    return await render_template('index_home.html')


@app.route('/.test/addr')
async def addr():
    return Response(util.get_ip_address(request), mimetype='text/plain')


@app.before_request
async def before_request_login():
    if current_app.config['COOKIE_KEY'] in request.cookies:
        xtsso_sesskey = request.cookies[current_app.config['COOKIE_KEY']]
        redis: aioredis.Redis = current_app.redis_pool
        g.cid = await redis.hget(f'session:{xtsso_sesskey}', 'cid')


@app.after_request
async def after_request_login(response):
    cfg = current_app.config
    if cfg['COOKIE_KEY'] in request.cookies:
        if g.cid is None:
            response.set_cookie(cfg['COOKIE_KEY'], '', max_age=-42,
                                path=cfg['COOKIE_PATH'], domain=cfg['COOKIE_DOMAIN'], secure=cfg['COOKIE_SECURE'],
                                httponly=cfg['COOKIE_HTTPONLY'])
        else:
            response.set_cookie(cfg['COOKIE_KEY'], request.cookies[cfg['COOKIE_KEY']], max_age=cfg['COOKIE_MAXAGE'],
                                path=cfg['COOKIE_PATH'], domain=cfg['COOKIE_DOMAIN'], secure=cfg['COOKIE_SECURE'],
                                httponly=cfg['COOKIE_HTTPONLY'])
    return response
